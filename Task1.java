import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task1 {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Employee employee1 = new Employee("Employee 1", 25, 5_000_000, createDate(2022, Calendar.FEBRUARY, 1),
                "Christian");
        Employee employee2 = new Employee("Employee 2", 28, 8_000_000, createDate(2022, Calendar.MARCH, 1),
                "Catholic");
        Employee employee3 = new Employee("Employee 3", 30, 12_000_000, createDate(2022, Calendar.APRIL, 1), "Muslim");

        executorService.execute(new SalaryCalculationTask(employee1));
        executorService.execute(new SalaryCalculationTask(employee2));
        executorService.execute(new SalaryCalculationTask(employee3));

        executorService.shutdown();
    }

    private static Date createDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    static class Employee {
        String name;
        int age;
        int salary;
        Date startDate;
        String religion;

        public int monthsWorked = 0;

        public Employee(String name, int age, int salary, Date startDate, String religion) {
            this.name = name;
            this.age = age;
            this.salary = salary;
            this.startDate = startDate;
            this.religion = religion;
        }
    }

    static class SalaryCalculationTask implements Runnable {
        Employee employee;

        public SalaryCalculationTask(Employee employee) {
            this.employee = employee;
        }

        @Override
        public void run() {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(employee.startDate);

            try {
                FileWriter fileWriter = new FileWriter(employee.name + ".txt");

                for (int i = calendar.get(Calendar.MONTH); i <= Calendar.DECEMBER; i++) {
                    employee.monthsWorked++;
                    calendar.set(Calendar.MONTH, i);
                    calendar.set(Calendar.DATE, 1);

                    fileWriter.write(
                            dateFormat.format(calendar.getTime()) + " - " + employee.salary + " - "
                                    + "monthly" + "\n");

                    int holidayPay = employee.salary / 12 * employee.monthsWorked;
                    boolean hasHoliday = false;
                    // Check THR
                    if (employee.religion.equals("Christian") || employee.religion.equals("Catholic")) {
                        if (i == Calendar.DECEMBER) {
                            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
                            calendar.set(Calendar.DATE, 11);
                            hasHoliday = true;
                        }
                    } else {
                        if (i == Calendar.APRIL) {
                            hasHoliday = true;
                        }
                    }

                    if (hasHoliday) {
                        fileWriter.write(
                                dateFormat.format(calendar.getTime()) + " - " + holidayPay + " - "
                                        + "THR" + "\n");
                    }

                }

                fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
