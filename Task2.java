import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task2 {

    public static void main(String[] args) {
        List<Product> products = createProducts(10);
        listProducts(products);
        simulateShopping(products);
    }

    public static List<Product> createProducts(int n) {
        List<Product> products = new ArrayList<>();
        Random random = new Random();

        for (int i = 1; i <= n; i++) {
            String name = "Product" + i;
            String description = "Product Desc" + i;
            double price = 5000 + random.nextDouble() * 5000;

            Price productPrice = new Price(i, price, "IDR", i);
            Product product = new Product(i, name, description, productPrice);
            products.add(product);
        }

        return products;
    }

    public static void listProducts(List<Product> products) {
        for (int i = 0; i < products.size(); i++) {
            System.out.println(i + ". " + products.get(i));
        }
    }

    public static void simulateShopping(List<Product> products) {
        Random random = new Random();
        double totalPrice = 0;
        List<Product> purchasedProducts = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            Product product = products.get(random.nextInt(products.size()));

            System.out.println(String.format("\nBeli %s", product));

            purchasedProducts.add(product);
            Price price = product.getPrice();

            if (countProducts(purchasedProducts, product) == 5) {
                double discountedPrice = (price.getAmount() * 0.8);
                System.out.println("Harga Diskon Produk " + product.getName() + "Rp. " + discountedPrice + "!");
                totalPrice += discountedPrice;
            } else if (countVariety(purchasedProducts) > 5) {
                purchasedProducts.add(getLowestPriceProduct(products));
                System.out.println(String.format("Gratis untuk produk %s", product));
            } else {
                totalPrice += price.getAmount();
            }
        }

        System.out.println("Total Price: " + totalPrice);
    }

    public static int countProducts(List<Product> products, Product product) {
        int count = 0;
        for (Product p : products) {
            if (p.getId() == product.getId()) {
                count++;
            }
        }
        return count;
    }

    public static int countVariety(List<Product> products) {
        ArrayList<Integer> uniqueIds = new ArrayList<>();
        for (Product p : products) {
            if (!uniqueIds.contains(p.getId())) {
                uniqueIds.add(p.getId());
            }
        }
        return uniqueIds.size();
    }

    public static Product getLowestPriceProduct(List<Product> products) {
        Product product = products.get(0);
        for (Product p : products) {
            if (p.price.getAmount() < product.price.getAmount()) {
                product = p;
            }
        }
        return product;

    }

    static class Product {
        private int id;
        private String name;
        private String description;
        private Price price;

        public Product(int id, String name, String description, Price price) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.price = price;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public Price getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return String.format("%s - Rp. %.2f", getName(), price.getAmount());
        }
    }

    static class Price {
        private int id;
        private double amount;
        private String currency;
        private int productId;

        public Price(int id, double amount, String currency, int productId) {
            this.id = id;
            this.amount = amount;
            this.currency = currency;
            this.productId = productId;
        }

        public double getAmount() {
            return amount;
        }
    }
}
